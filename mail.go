package mail

import (
	"fmt"
	"net/smtp"
)

//Mail empty struct with Info() and Error() as methods
type Mail struct {
	//smtpUsername username of smtp server for sending mail
	smtpUsername string
	//SMTPPassword pwd of smtp server
	smtpPassword string
	//SMTPServer server name
	smtpServer string
	//SMTPTLSPort server port
	smtpTLSPort int16
	//MailTo email of sysadmin for sending logs
	mailTo string
	//MailFrom email id of app
	mailFrom string

	lg Logger
}

type MailMsg struct {
	Subject string
	Body string
}

//Config config for mail
type Config interface {
	//SMTPUsername username of smtp server for sending mail
	SMTPUsername() string
	//SMTPPassword pwd of smtp server
	SMTPPassword() string
	//SMTPServer server name
	SMTPServer() string
	//SMTPTLSPort server port
	SMTPTLSPort() int16
	//MailTo email of sysadmin for sending logs
	MailTo() string
	//MailFrom email id of app
	MailFrom() string
}

//New returns pointer to instance of Mail. Pass in config and logger here
func New(cfg Config, log Logger) *Mail {
	var m Mail
	m.smtpUsername = cfg.SMTPUsername()
	m.smtpPassword = cfg.SMTPPassword()
	m.smtpServer = cfg.SMTPServer()
	m.smtpTLSPort = cfg.SMTPTLSPort()
	m.mailFrom = cfg.MailFrom()
	m.mailTo = cfg.MailTo()
	m.lg = log
	return &m
}

//Info sends info messages to email specified in Config.mailTo
func (m *Mail) Info(info MailMsg) {
	// prefix := "Info"
	prefix := ""
	createSend(m, prefix, info)
}

//Error sends error message to email specified in Config.mailTo. pass in type error
func (m *Mail) Error(e error) {

	errorMessage := MailMsg{
		Subject: "Resource Srv Error",
		Body:e.Error(),
	}
	createSend(m, "Error", errorMessage)

}

func createSend(m *Mail, messageType string, message MailMsg) {
	msg := createMessage(m, messageType, message)
	sendMessage(m, msg)
}

func createMessage(m *Mail, messageType string, message MailMsg) []byte {
	var subject string

	//form subject from message
	if len(message.Subject) < 20 {
		subject = message.Subject
	} else {
		subject = message.Subject[:20]
	}

	//mail is marked as spam by Gmail/Prontonmail if fields not formatted into separate lines. must be on separate lines.
	// not sure why
	from := fmt.Sprintf("From: %s\r\n", m.mailFrom)
	to := fmt.Sprintf("To: %s\r\n", m.mailTo)
	sub := fmt.Sprintf("Subject: %s %s\r\n", messageType, subject)
	// body := fmt.Sprintf("\r\n%v \r\n%s", time.Now(), message)
	body := fmt.Sprintf("\r\n%s", message.Body)

	messageSubjectandBody := from + to + sub + body

	msg := []byte(messageSubjectandBody)
	return msg
}

func sendMessage(m *Mail, msg []byte) {
	m.lg.Debug("Sending mail....")
	// Choose auth method and set it up
	auth := smtp.PlainAuth("", m.smtpUsername, m.smtpPassword, m.smtpServer)
	// Here we do it all: connect to our server, set up a message and send it
	to := []string{m.mailTo}
	from := m.mailFrom
	err := smtp.SendMail(m.smtpServer+":"+fmt.Sprint(m.smtpTLSPort), auth, from, to, msg)

	if err != nil {
		m.lg.Info(err)
	}

}
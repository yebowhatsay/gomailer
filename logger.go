package mail

//Logger interface
type Logger interface {
	Info(args ...interface{})
	Infof(s string, args ...interface{})
	Error(args ...interface{})
	Fatal(args ...interface{})
	Debug(args ...interface{})
}
